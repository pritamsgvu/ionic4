import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.page.html',
  styleUrls: ['./alerts.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class AlertsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
