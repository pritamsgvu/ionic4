import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class LoginPage {


  constructor(public menuCtrl: MenuController, private router: Router, private nav: NavController) {
    this.menuCtrl.enable(false);

  }

  goToPage() {
    // this.nav.navigateRoot(["/home"]);
    // this.router.navigate(["/home"]);
    this.router.navigateByUrl("/home");

  }


}
