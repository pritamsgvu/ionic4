import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WardInfoPage } from './ward-info.page';

describe('WardInfoPage', () => {
  let component: WardInfoPage;
  let fixture: ComponentFixture<WardInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WardInfoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WardInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
