import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  encapsulation:ViewEncapsulation.None
})

export class HomePage {

  constructor(private router: Router) {

  }

  goToPage() {
    this.router.navigate(["/list"]);
  }

}
