import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsPageModule'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'alerts', loadChildren: './alerts/alerts.module#AlertsPageModule' },
  { path: 'users', loadChildren: './users/users.module#UsersPageModule' },
  { path: 'invite-guests', loadChildren: './invite-guests/invite-guests.module#InviteGuestsPageModule' },
  { path: 'messages', loadChildren: './messages/messages.module#MessagesPageModule' },
  { path: 'ward-info', loadChildren: './ward-info/ward-info.module#WardInfoPageModule' },
  { path: 'status-update', loadChildren: './status-update/status-update.module#StatusUpdatePageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
