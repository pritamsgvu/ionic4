import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ListPage implements OnInit {
  private selectedItem: any;

  constructor(private router: Router) {
 
  }

  ngOnInit() {
  }
  

  goToPage(){
    this.router.navigate(["/forms"]);
  }
}
