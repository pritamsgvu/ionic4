import { Component } from '@angular/core';

import { Platform ,MenuController} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [

    {
      title: 'List',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },

    {
      title: 'Form',
      url: '/forms',
      icon: 'clipboard'
    },

    {
      title: 'Alerts',
      url: '/alerts',
      icon: 'clipboard'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  closeMenu() {
    this.menu.close();
  }
}
