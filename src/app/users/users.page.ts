import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class UsersPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
