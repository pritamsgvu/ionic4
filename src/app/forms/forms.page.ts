import { Component,OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.page.html',
  styleUrls: ['./forms.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class FormsPage {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPage() {
    this.router.navigate(["/alerts"]);
  }
}
