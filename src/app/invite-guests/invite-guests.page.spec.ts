import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteGuestsPage } from './invite-guests.page';

describe('InviteGuestsPage', () => {
  let component: InviteGuestsPage;
  let fixture: ComponentFixture<InviteGuestsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteGuestsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteGuestsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
