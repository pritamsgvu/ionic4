import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-invite-guests',
  templateUrl: './invite-guests.page.html',
  styleUrls: ['./invite-guests.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class InviteGuestsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
