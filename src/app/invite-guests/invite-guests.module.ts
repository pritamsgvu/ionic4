import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InviteGuestsPage } from './invite-guests.page';

const routes: Routes = [
  {
    path: '',
    component: InviteGuestsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InviteGuestsPage]
})
export class InviteGuestsPageModule {}
